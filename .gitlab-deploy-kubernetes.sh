#!/bin/bash
WAKTU=$(date '+%Y-%m-%d.%H')
echo "$SSH_KEY" > key.pem
chmod 400 key.pem

if [ "$1" == "BUILD" ];then
echo '[*] Building Program To Docker Images'
echo "[*] Tag $WAKTU"
docker build -t strongpapazola/mydockerapps:$CI_COMMIT_BRANCH .
docker login --username=$DOCKER_USER --password=$DOCKER_PASS
docker push strongpapazola/mydockerapps:$CI_COMMIT_BRANCH
echo $CI_PIPELINE_ID

elif [ "$1" == "DEPLOY" ];then
echo "[*] Tag $WAKTU"
echo "[*] Deploy to production server in version $CI_COMMIT_BRANCH"
echo '[*] Generate SSH Identity'
HOSTNAME=`hostname` ssh-keygen -t rsa -C "$HOSTNAME" -f "$HOME/.ssh/id_rsa" -P "" && cat ~/.ssh/id_rsa.pub
echo '[*] Execute Remote SSH'
ssh -i key.pem -o "StrictHostKeyChecking no" root@34.171.203.66 "microk8s.kubectl delete -f deployment.yaml"
ssh -i key.pem -o "StrictHostKeyChecking no" root@34.171.203.66 "cp TEMPLATE.yaml deployment.yaml"
ssh -i key.pem -o "StrictHostKeyChecking no" root@34.171.203.66 "sed -i 's/IMAGEPRODUCT/strongpapazola\/mydockerapps:$CI_COMMIT_BRANCH/g' deployment.yaml"
ssh -i key.pem -o "StrictHostKeyChecking no" root@34.171.203.66 "microk8s.kubectl apply -f deployment.yaml"
echo $CI_PIPELINE_ID
fi
